Task 12 Exercise 2
Task 2

2a. Discovering soft and hard links. Comment on results of these commands (place the output into your report):

cd

mkdir test

cd test

touch test1.txt

echo “test1.txt” > test1.txt

ls -l . (a hard link)

ln test1.txt test2.txt

ls -l .

(pay attention to the number of links to test1.txt and test2.txt)

echo “test2.txt” > test2.txt

cat test1.txt test2.txt

rm test1.txt

ls -l .

(now a soft link)

ln -s test2.txt test3.txt

ls -l .

(pay attention to the number of links to the created files)

rm test2.txt; ls -l .



2b. Dealing with chmod.

An executable script. Open your favorite editor and put these lines into a file

#!/bin/bash

echo “Drugs are bad MKAY?”



Give name “script.sh” to the script and call to

chmod +x script.sh

Then you are ready to execute the script:

./script.sh