"""
Task 2_3

You are given n bars of gold with weights: w1, w2, ..., wn and bag with capacity W.
There is only one instance of each bar and for each bar you can either take it or not
(hence you cannot take a fraction of a bar). Write a function that returns the maximum weight of gold that fits
into a knapsack's capacity.

The first parameter contains 'capacity' - integer describing the capacity of a knapsack
The next parameter contains 'weights' - list of weights of each gold bar
The last parameter contains 'bars_number' - integer describing the number of gold bars
Output : Maximum weight of gold that fits into a knapsack with capacity of W.

Note:
Use the argparse module to parse command line arguments. You don't need to enter names of parameters (i. e. -capacity)
Raise ValueError in case of false parameter inputs
Example of how the task should be called:
python3 task3_1.py -W 56 -w 3 4 5 6 -n 4
"""


import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-W', type=int, help='capacity')
parser.add_argument('-w', type=int, nargs='+', help='weights')
parser.add_argument('-n', type=int, help='num_bars')


def bounded_knapsack(args):
    capacity = args.W
    weights = args.w
    num_bars = args.n

    recur_func = [1] + [0] * capacity
    for i in range(num_bars):
        for j in range(capacity, weights[i] - 1, -1):
            if recur_func[j - weights[i]] == 1:
                recur_func[j] = 1

    j = capacity
    while recur_func[j] == 0:
        j -= 1
    return j


def main():
    args = parser.parse_args()
    if args.W <= 0 or args.n <= 0 or args.n != len(args.w) or any(x < 0 for x in args.w):
        raise ValueError
    print(bounded_knapsack(args))


if __name__ == '__main__':
    main()



