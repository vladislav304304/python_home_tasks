"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""


import argparse

parser = argparse.ArgumentParser(description='Simple arithmetic operations')

parser.add_argument('operand_1', type=float, help='operand_1')
parser.add_argument('operator', type=str, help='operator')
parser.add_argument('operand_2', type=float, help='operand_2')


def calculate(args):
    operand_1 = args.operand_1
    operator = args.operator
    operand_2 = args.operand_2

    arithm_operations = {
        '+': lambda first, second: first + second,
        '-': lambda first, second: first - second,
        '*': lambda first, second: first * second,
        '/': lambda first, second: first / second
    }

    if operator in arithm_operations:
        return arithm_operations[operator](operand_1, operand_2)
    else:
        raise NotImplementedError


def main():
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()



# import operator
# import argparse
#
#
# parser = argparse.ArgumentParser(description='Simple arithmetic operations')
#
# parser.add_argument('operand_1', type=float, help='operand_1')
# parser.add_argument('operation', type=str, help='operation')
# parser.add_argument('operand_2', type=float, help='operand_2')
#
#
# def calculate(args):
#     operand_1 = args.operand_1
#     operation = args.operation
#     operand_2 = args.operand_2
#
#     arithm_operations = {
#         '+': lambda first, second: operator.add(first, second),
#         '-': lambda first, second: operator.sub(first, second),
#         '*': lambda first, second: operator.mul(first, second),
#         '/': lambda first, second: operator.truediv(first, second)
#     }
#
#     if operation in arithm_operations:
#         return arithm_operations[operation](operand_1, operand_2)
#     else:
#         raise NotImplementedError
#
#
# def main():
#     args = parser.parse_args()
#     print(calculate(args))
#
#
# if __name__ == '__main__':
#     main()