"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""

import operator
import math
import argparse

parser = argparse.ArgumentParser(description='Standard math functions')
parser.add_argument('input', type=str, nargs='*', help='function, arg1, arg2')


def calculate(args):
    function_str = args.input[0]

    if hasattr(math, function_str):
        function = getattr(math, function_str)
    elif hasattr(operator, function_str):
        function = getattr(operator, function_str)
    else:
        raise NotImplementedError

    if len(args.input) == 3:
        arg_1, arg_2 = map(float, args.input[1:])
        return function(arg_1, arg_2)
    elif len(args.input) == 2:
        arg_1 = float(args.input[1])
        return function(arg_1)
    elif len(args.input) == 1:
        return function
    else:
        raise TypeError


def main():
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
