""" Write a Python-script that determines whether the input string is the correct entry for the
'formula' according EBNF syntax (without using regular expressions).
Formula = Number | (Formula Sign Formula)
Digit = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
Number = Digit{Digit}
Sign = '+' | '-'
Input: string
Result: (True / False, The result value / None)

Example,
user_input = '1+2+4-2+5-1' result = (True, 9)
user_input = '123' result = (True, 123)
user_input = '-123' result = (False, None)
user_input = 'hello+12' result = (False, None)
user_input = '2++12--3' result = (False, None)
user_input = '' result = (False, None)

Example how to call the script from CLI:
python task_1_ex_3.py 1+5-2

Hint: use argparse module for parsing arguments from CLI
"""

import argparse

parser = argparse.ArgumentParser(description='Parsing string according to the formula')
parser.add_argument('input_string', type=str, help='Input string for checking')


def check_formula(input_string):
    # check that string is not empty, begins and ends with digit
    if not input_string or not input_string[0].isdigit() or not input_string[-1].isdigit():
        return False, None

    # check that the string contains digits and signs "+", "-" only
    acceptable_symbols = set([str(x) for x in range(10)] + ['+', '-'])
    for char in input_string:
        if char not in acceptable_symbols:
            return False, None

    # check that string does not contain double signs: '++', '--', '+-', '-+'
    double_signs = {'++', '--', '+-', '-+'}
    for ds in double_signs:
        if ds in input_string:
            return False, None

    # calculate result if the string complies with the formula
    multiplier = {'+': 1, '-': -1}
    res = 0
    cur_sign = '+'
    cur_num = ''

    for char in input_string:
        if char.isdigit():
            cur_num += char
        else:
            res += int(cur_num) * multiplier[cur_sign]
            cur_num = ''
            cur_sign = char
    res += int(cur_num) * multiplier[cur_sign]
    return True, res


def main():
    args = parser.parse_args()
    print(check_formula(args.input_string))


if __name__ == '__main__':
    main()
