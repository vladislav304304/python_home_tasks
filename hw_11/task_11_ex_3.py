"""
Task 3

Implement a decorator `call_once` which runs `sum_of_numbers` function once and caches the result.
All consecutive calls to this function should return cached result no matter the arguments.

Example:
@call_once
def sum_of_numbers(a, b):hw_8
    return a + b

print(sum_of_numbers(13, 42))

>>> 55

print(sum_of_numbers(999, 100))

>>> 55

print(sum_of_numbers(134, 412))

>>> 55
"""

first_call = None


def call_once(fn):
    """
    decorator which runs `sum_of_numbers` function once and caches the result.
    All consecutive calls to this function should return cached result no matter the arguments
    """

    def wrapper(*args):
        global first_call
        if first_call is None:
            first_call = fn(*args)
        return first_call
    return wrapper


@call_once
def sum_of_numbers(a, b):
    return a + b
