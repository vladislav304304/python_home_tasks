"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
>>> ['donec', 'etiam', 'aliquam']
> NOTE: Remember about dots, commas, capital letters etc.
"""

from collections import Counter
import re


def most_common_words(text, top_words):
    with open(text) as text_file:
        without_punctuation = re.sub("[.,!?]", "", text_file.read()).strip()

    most_common = Counter(without_punctuation.split()).most_common(top_words)
    return [word for word, count in most_common]

#
# print(most_common_words('test.txt', 4))
