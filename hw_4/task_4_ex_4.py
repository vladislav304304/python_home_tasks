"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`. 
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
# >>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

# >>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
# ["python", "is", "cool", ",", "isn't", "it?"]
#
# >>> split_by_index("no luck", [42])
["no luck"]
```
"""


def valid_indexes(indexes):
    """Leave in the list positive ints, larger than previous in the list"""
    positive_ints = list(filter(lambda x: isinstance(x, int) and x > 0, indexes))
    if not positive_ints:
        return []
    prepared_indexes = [positive_ints[0]]
    for i in range(1, len(positive_ints)):
        if positive_ints[i] > positive_ints[i - 1]:
            prepared_indexes.append(positive_ints[i])
    return prepared_indexes


def split_by_index(string, indexes):
    """function `split_by_index(string: str, indexes: List[int]) -> List[str]`
    which splits the `string` by indexes specified in `indexes`"""
    prepared_indexes = valid_indexes(indexes)
    if not prepared_indexes:
        return [string]
    split_list = []
    prev_index = 0
    for index in prepared_indexes:
        if index < len(string):
            split_list.append(string[prev_index:index])
            prev_index = index
        else:
            break
    split_list.append(string[prev_index:])
    return split_list



