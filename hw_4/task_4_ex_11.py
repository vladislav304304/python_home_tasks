"""
Write a function `fibonacci_loop(seq: list)`, which accepts a list of values and
prints out values in one line on these conditions:
 - floating point numbers should be ignored
 - string values should stop the iteration
 - loop control statements should be used

Example:
>>> fibonacci_loop([0, 1, 1.1, 1, 2, 99.9, 3, 0.0, 5, 8, "stop", 13, 21, 34])
0 1 1 2 3 5 8
"""


def fibonacci_loop(seq):
    """accepts a list of values and
    prints out values in one line on these conditions:
    - floating point numbers should be ignored
    - string values should stop the iteration
    - loop control statements should be used
    """
    # form list of ints before str el in seq
    int_list = []
    for el in seq:
        if type(el) is int:
            int_list.append(el)
        elif isinstance(el, str):
            break  # stop iteration
    # form fibo_list from int_list    
    fibo_list = int_list[:2]
    for i in range(2, len(int_list)):
        fibo_list.append(int_list[i - 2] + int_list[i - 1])
    # form string of elements of fibo_list and print in 1 line:
    print(' '.join(map(str, fibo_list)))
