"""
Task04_1_2
Write `is_palindrome` function that checks whether a string is a palindrome or not
Returns 'True' if it is palindrome, else 'False'

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).

Note:
Usage of any reversing functions is prohibited.
The function has to ignore special characters, whitespaces and different cases
Raise ValueError in case of wrong data type
"""


def is_palindrome(test_string: str) -> bool:
    """ this function checks whether a string is a palindrome
    or not Returns 'True' if it is palindrome, else 'False'
    """
    # checking if argument is a string:
    if not isinstance(test_string, str):
        raise ValueError

    # receive only letters from string in lowercase
    only_letters = []
    for char in test_string.lower():
        if char.isalpha():
            only_letters.append(char)

    # checking for palindrome
    for i in range(0, len(only_letters) // 2 + 1):
        if only_letters[i] != only_letters[-1 - i]:
            return False
    return True






