"""
Task04_1_7
Implement a function foo(List[int]) -> List[int] which, given a list of integers, returns a new  or modified list
in which every element at index i of the new list is the product of all the numbers in the original array except the one at i.
Example:
`python

foo([1, 2, 3, 4, 5])
[120, 60, 40, 30, 24]

foo([3, 2, 1])
[2, 3, 6]`
"""

from typing import List


def product_array(num_list: List[int]) -> List[int]:
    """function which, given a list of integers, returns a new  or modified list
    in which every element at index i of the new list is the product of all the numbers
    in the original array except the one at i
    """
    # receive full product of the elements  !=0
    full_product = 1
    for el in num_list:
        if el != 0:
            full_product *= el
    # receive required list for 3 cases:
    # 1) if the list contain 2 or more zero elements
    if num_list.count(0) > 1:
        return [0] * len(num_list)
    # 2) if the list contain 1 zero element
    elif num_list.count(0) == 1:
        zero_index = num_list.index(0)
        return [0] * zero_index + [full_product] + [0] * (len(num_list) - zero_index - 1)
    # 2) if there is no zero elements in the list
    else:
        return list(map(lambda x: int(full_product / x), num_list))
