"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def swap_quotes(string: str) -> str:
    str_list = list(string)
    for i, v in enumerate(str_list):
        if v == '"':
            str_list[i] = "'"
        elif v == "'":
            str_list[i] = '"'
    return ''.join(str_list)




