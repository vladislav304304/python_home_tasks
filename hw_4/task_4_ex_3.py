"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""


def split_alternative(str_to_split: str, delimiter: str = " ") -> list:
    """function that works the same as str.split"""
    if not isinstance(str_to_split, str) or not isinstance(delimiter, str) or not delimiter:
        raise ValueError

    list_from_string = []
    cur_el = ''
    for char in str_to_split:
        if char != delimiter:
            cur_el += char
        else:
            if cur_el:
                list_from_string.append(cur_el)
            cur_el = ''
    if cur_el:
        list_from_string.append(cur_el)
    return list_from_string



