"""
For a positive integer n calculate the result value, which is equal to the sum
of the odd numbers of n.

Example,
n = 1234 result = 4
n = 246 result = 0

Write it as function.

Note:
Raise TypeError in case of wrong data type or negative integer;
Use of 'functools' module is prohibited, you just need simple for loop.
"""


def sum_odd_numbers(n: int) -> int:
    """function calculates for a positive integer n  
    the result value, which is equal to the sum
    of the odd numbers of n
    """
    if type(n) != int or n < 0:
        raise TypeError
    return sum(filter(lambda x: x % 2 == 1, map(int, str(n))))


