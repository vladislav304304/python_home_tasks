"""
Implement function count_letters, which takes string as an argument and
returns a dictionary that contains letters of given string as keys and a
number of their occurrence as values.

Example:
print(count_letters("Hello world!"))
Result: {'H': 1, 'e': 1, 'l': 3, 'o': 2, 'w': 1, 'r': 1, 'd': 1}

Note: Pay attention to punctuation.
"""

from collections import Counter


def count_letters(string):
    if not isinstance(string, str):
        raise TypeError
    all_letters_list = [char for char in string if char.isalpha()]
    return dict(Counter(all_letters_list))
