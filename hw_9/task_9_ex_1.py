"""
Task_9_1
Implement `swap_quotes` function which receives a string and replaces all " symbols with ' and vise versa.
The function should return modified string.

Note:
Usage of built-in or string replacing functions is required.
"""


# Not found usage of required constructions
# ".translate, maketrans, .replace


def swap_quotes(some_string: str) -> str:
    tr_dict = {ord("'"): ord('"'), ord('"'): ord("'")}
    some_string = some_string.translate(tr_dict)
    return some_string
