"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
  Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
  Note: use `string.ascii_lowercase` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
test_strings = ["hello", "world", "python", ]
print(chars_in_all(*test_strings))
>>> {'o'}
print(chars_in_one(*test_strings))
>>> {'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(chars_in_two(*test_strings))
>>> {'h', 'l', 'o'}
print(not_used_chars(*test_strings))
>>> {'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}
"""

from typing import Tuple
from functools import reduce
import string


def check_data_type(args: Tuple) -> None:
    """function checks that all the
    arguments of base function are string,
    if not -> raise ValueError"""
    for arg in args:
        if not isinstance(arg, str):
            raise TypeError


def chars_in_all(*strings):
    check_data_type(strings)
    strings_sets = map(set, strings)
    return reduce(lambda x, y: x & y, strings_sets)


def chars_in_one(*strings):
    check_data_type(strings)
    strings_sets = map(set, strings)
    return reduce(lambda x, y: x | y, strings_sets)


def chars_in_two(*strings):
    check_data_type(strings)
    if len(strings) < 2:
        raise ValueError
    result = set()
    sets_list = list(map(set, strings))
    length = len(sets_list)
    for i in range(length - 1):
        for j in range(i + 1, length):
            result |= sets_list[i] & sets_list[j]
    return result


def not_used_chars(*strings):
    check_data_type(strings)
    all_letters = set(''.join([el_string.lower() for el_string in strings if el_string.isalpha()]))
    alphabet = set(string.ascii_lowercase)
    return alphabet - all_letters
