"""
Implement function combine_dicts, which receives a changeable
number of dictionaries (keys - letters, values - integers)
and combines them into one dictionary.

Dict values should be summarized in case of identical keys.

Example:
dict_1 = {'a': 100, 'b': 200}
dict_2 = {'a': 200, 'c': 300}
dict_3 = {'a': 300, 'd': 100}

combine_dicts(dict_1, dict_2)
Result: {'a': 300, 'b': 200, 'c': 300}

combine_dicts(dict_1, dict_2, dict_3)
Result: {'a': 600, 'b': 200, 'c': 300, 'd': 100}
"""

from typing import Dict


from typing import Dict


def combine_dicts(*dicts) -> Dict:
    """function receives a changeable
    number of dictionaries (keys - letters, values - integers)
    and combines them into one dictionary."""
    common_dict = {}
    for cur_dict in dicts:
        if isinstance(cur_dict, dict):
            for k, v in cur_dict.items():
                if not isinstance(k, str) or len(k) != 1 or not k.isalpha():
                    raise KeyError
                elif type(v) is not int:
                    raise ValueError
                common_dict[k] = common_dict.get(k, 0) + v
        else:
            raise TypeError
    return common_dict



